<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;
use App\User;

class CustomerController extends Controller
{
    public function validateAcc($msisdn)
    {
        return $this->validateAccount($msisdn);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'region_code' => 'required',
            'coordinates' => 'required',
            'postal_code' => 'required',
            'city_code' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'password' => 'required'
        ]);

        $prepared = [];
        
        $prepared['subscriber']['password'] = $request->password; 
        $prepared['subscriber']['middle-name'] = ($request->middle_name) ? $request->middle_name : ''; 
        $prepared['subscriber']['valid-id-desc'] = 'EMAIL';
        
        $prepared['subscriber']['resident-address']['specific-address'] = $request->address;
        $prepared['subscriber']['resident-address']['region-code'] = $request->region_code;
        $prepared['subscriber']['resident-address']['coordinates'] = ($request->coordinates) ? $request->coordinates : '';
        $prepared['subscriber']['resident-address']['postal-code'] = $request->postal_code;
        $prepared['subscriber']['resident-address']['city-code'] = $request->city_code;
        
        $prepared['subscriber']['business-name'] = ($request->business_name) ? $request->business_name : '';
        $prepared['subscriber']['valid-id'] = $request->email;
        $prepared['subscriber']['account-name'] = "{$request->first_name}, {$request->last_name}";
        $prepared['subscriber']['authorized-mobile'] = $request->mobile;
        $prepared['subscriber']['authorized-email'] = $request->email;
        $prepared['subscriber']['first-name'] = $request->first_name;
        $prepared['subscriber']['last-name'] = $request->last_name;

        $prepared['auth']['password'] = '1234';
        $prepared['request-id'] = Str::random(32);
        
        $postToKaspro = $this->registerAccount($prepared);

        $statusCode = 0;

        if ($postToKaspro) {
            $statusCode = $postToKaspro->getStatusCode();

            $content = (array) json_decode($postToKaspro->getBody()->getContents());

            if (array_key_exists('account-number', $content)) {

                try {

                    $user = new User;
                    $user->first_name = $request->first_name;
                    $user->last_name = $request->last_name;
                    $user->email = $request->email;
                    $user->phone_number = $request->mobile;

                    $user->partner_token = $content["partner-token"];
                    $user->account_number = $content["account-number"];

                    $user->save();

                    return response()->json(['account_number' => $user->account_number, 'code' => 200], $statusCode);
                    
                } catch (\Exception $e) {
                    return response()->json($e);
                }
            } else {

                return response()->json($content, $statusCode);
            }
        }
    }

    public function wallet($msisdn)
    {
        $validates = $this->validateAccount($msisdn);

        if ($validates['value']['status'] == 200) {

            $user = User::where(['phone_number' => $msisdn])->first();

            if ($user) {

                $inquiry = $this->accountInquiry([
                    'account_number' => $user->account_number,
                    'phone_number' => $user->phone_number,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name
                ]);

                $bodyInquiry = $inquiry->getBody();

                return response()->json([
                    'status' => $inquiry->getStatusCode(),
                    'data' => json_decode($bodyInquiry->getContents())
                ], $inquiry->getStatusCode());
            }
        }

        return response()->json([
            'status' => $validates['value']['status'],
            'data' => json_decode($validates['value']['content'])
        ], $validates['value']['status']);
    }

    public function getOtp($mssidn)
    {
        return response()->json($this->__getOtp($mssidn));
    }
}
