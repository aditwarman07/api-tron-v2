<?php

namespace App\Http\Middleware;

use Closure;

class CrossPlatform
{
    /**
     * Ensures that there is an App header present, and that is
     * matches the env setting.
     *
     * Set APP_ID in your .env
     * Request with `App: your-key-here`
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string                   $role
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $appId = env('APP_ID');

        $crossPlatform = [
            env('AGENT_ACCESS'),
            env('MOBILE_ACCESS'),
            env('WEB_ACCESS'),
        ];
        
        // Ensure that the requesting app is legit
        if (!is_null($appId) && (in_array($request->header('App'), $crossPlatform))) {
            return $next($request);
        }

        throw new \Exception('There was a problem validating the request.');
    }
}
