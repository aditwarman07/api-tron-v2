<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7;
use Illuminate\Support\Str;

use App\User;

trait Kaspro
{
    protected $url = 'http://dev.kaspro.id/api/';

    private $token = 'WLu28cXFYvrdtQ7KFNxDUI3hpufmj+EbNknAEL9i7pfdjx69s/lnu3YSScaxUv+7Iere9Or5f1AvNC3rO8l+U3gkcU87vUrlHu6llGJeZiolpM2mD1ZePTlPyjVrArkmlK5Ui8vnGmu55anh2jq2Y4KD9HIj2FI8ENzfFqPX3/vmVH2e8ImkxsDuK1Ot+oH6BVxUKThhqcVPFfv3Qe52AA==';

    private $account_number = '923732980516';

    private $required_param_payment = ['account_number', 'destination', 'amount', 'reference'];

    /**
     * @param array $credential
     * @return object
     */
    public function signIn(Array $credential)
    {
        $request = $this->__prepared()->get("{$credential}/payu/session", [
            'headers' => [
                'Authorization' => "Basic {$credential['pin']}"
            ]
        ]);

        return $request;
    }

    /**
     * @param array $account
     * @return object
     */
    public function accountInquiry(Array $account)
    {
        $request = $this->__prepared()->get("{$account['account_number']}/partner/subscriber/wallet", [
            'query' => [
                'msisdn' => $account['phone_number'],
                'firstName' => $account['first_name'],
                'lastName' => $account['last_name']
            ]
        ]);

        return $request;
    }
    
    /**
     * @param accountInfo 
     * @return object
     */
    public function registerAccount(Array $accountInfo)
    {
        $body = json_encode($accountInfo);
        $accountNumber = env('WALLET_TRONCORP_ACCOUNT_ID');

        $request = $this->__prepared()->post("{$accountNumber}/partner/subscribers", ['body' => $body]);

        return $request;
    }

    /**
     * @param msisdn Customer phone number
     */
    public function validateAccount($msisdn)
    {
        $promise = $this->__prepared()->getAsync("{$msisdn}/validate")->then(function (ResponseInterface $response) use ($msisdn) {
            if ($response->getStatusCode() == 200) {
                $user = User::where(['phone_number' => $msisdn])->first();

                if ($user) {
                    return [
                        'status' => $response->getStatusCode(),
                        'content' => $user,
                    ];
                } else {
                    return [
                        'status' => 404,
                        'content' => 'Data tidak ditemukan'
                    ];
                }
            }
        }, function (RequestException $e) {
            return [
                'status' => $e->getResponse()->getStatusCode(),
                'content' => $e->getResponse()->getBody()->getContents(),
            ];
        });

        $response = Promise\settle($promise)->wait();
        
        return $response[0];
    }

    public function payment(Array $store)
    {
        $storeToKaspro = [
            'payments' => [
                [
                    "pocket-id" => "2",
                    "amount" => $store['amount'],
                    "reference" => $store['reference']
                ]
            ],
            'auth' => [
                'pass' => $store["password"]
            ],
            'destination' => $store['destination'],
            'request-id' => Str::random(32)
        ];

        $request = $this->__prepared()->postAsync("{$store['account_number']}/kaspro/transfers", ['body' => json_encode($storeToKaspro)]);

        $request->then(function (ResponseInterface $response) {
            $statCode = $response->getStatusCode();

            if ($response->getStatusCode() == 200) {
                return [
                    'status' => $response->getStatusCode(),
                    'data' => json_decode($response->getBody()->getContents())
                ];
            }
        }, function (RequestException $e) {
            return [
                'status' => $e->getResponse()->getStatusCode(),
                'message' => $e->getMessage()
            ];
        });

        $response = Promise\settle($request)->wait();
        
        return $response[0];
    }

    public function sendToMerchant(Array $account)
    {
        $storeToKaspro = [
            'payments' => [
                [
                    "pocket-id" => "2",
                    "amount" => $account['amount'],
                    "reference" => $account['reference']
                ]
            ],
            'auth' => [
                'pass' => $account["password"],
                "partner_token" => $account["partner_token"]
            ],
            'destination' => $account['destination'],
            'request-id' => Str::random(32)
        ];
        
        $request = $this->__prepared()->post("{$account['account_number']}/payments/mpqr", [
            'headers' => [
                'Accept-Language' => 'ID'
            ],
            'body' => json_encode($storeToKaspro)
        ]);

        return $request;
    }

    protected function __prepared()
    {
        $client = new Client([
            'base_uri' => $this->url,
            'headers' => [
                'Content-Type' => 'application/json',
                "cache-control" => "no-cache",
                "Token" => $this->token
            ]
        ]);

        return $client;
    }

    /**
     * @param mssidn the mobile number of the subscriber
     * @return array
     */
    protected function __getOtp($msisdn)
    {
        try {
            $request = $this->__prepared()->get("{$msisdn}/register/otp");
            
            if ($request->getStatusCode() == 200) {
                $data = json_decode($request->getBody()->getContents());
                
                $user = User::where(['phone_number' => $msisdn])->first();
                if ($user) {

                    $user->otp = $data->otp;
    
                    if ($user->save()) {
                        return $data;
                    }
                } else {
                    return $data;
                }
            }
        } catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }
}
