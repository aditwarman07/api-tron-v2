<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique(); // ledgers id
            $table->uuid('_puid')->unique(); // payment provider id
            $table->uuid('transaction_id')->unique();
            $table->uuid('account_id_from');
            $table->double('amount');
            $table->text('description');
            $table->boolean('is_sync');

            $table->datetime('sync_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ledgers');
    }
}
