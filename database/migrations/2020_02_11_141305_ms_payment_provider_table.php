<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MsPaymentProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_payment_providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique(); // master payment provider id
            $table->string('name', 20);
            $table->string('code', 20);
            $table->text('ucode');
            $table->boolean('is_actived');
            $table->text('url'); // API end point Payment Provider
            $table->text('etc');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_payment_providers');
    }
}
