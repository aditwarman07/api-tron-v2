<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MsAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique(); // master account id
            $table->uuid('_uid')->unique(); // user id
            $table->uuid('_puid')->unique(); // payment provider id
            $table->string('account_number', 20);
            $table->string('account_name', 32);
            $table->string('current_balance', 32);
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_accounts');
    }
}
