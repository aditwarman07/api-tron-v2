<?php

$router->group(
    [
        'middleware' => ['nocache', 'hideserver', 'security', 'csp', 'cors', 'auth:api', 'throttle'],
    ],
    function () use ($router) {
        /*
        * Admin Routes
        */
        $router->group(['prefix' => 'payment'], function ($router) {
            $router->get('version', function () use ($router) {
                return response()->json([
                    'data' => [
                        "version" => "1.2"
                    ]
                ]);
            });
        });
    }
);
